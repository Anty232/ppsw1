#include <LPC21xx.H>
#include "led.h"
#include "timer_interrupts.h"
#include "servo.h"

#define DETECTOR_bm (1<<10)
//git test



void DetectorInit(void){
	IO0DIR=IO0DIR&(~DETECTOR_bm);
	
}

enum DetectorState {ACTIVE, INACTIVE};
enum DetectorState eReadDetector()
{
  if ((IO0PIN & DETECTOR_bm) == 0) 
	{
    return ACTIVE;
  }
	else
	{
		return INACTIVE;
	}
}

enum ServoState{CALLIB,IDLE,IN_PROGRESS};

struct Servo
{
	enum ServoState eState; 
	unsigned int uiCurrentPosition; 
	unsigned int uiDesiredPosition;
}; 
struct Servo sServo;






enum ServoState eState = CALLIB;

void Automat() 
{
	
	switch(sServo.eState)
			{
				case IDLE:
					if(sServo.uiCurrentPosition==sServo.uiDesiredPosition)
					{
						sServo.eState=IDLE;
					}
					else 
					{
						sServo.eState=IN_PROGRESS;
					}
					break;
					
				case IN_PROGRESS: 	
					if(sServo.uiCurrentPosition<sServo.uiDesiredPosition)
					{
						LedStepRight();
						sServo.uiCurrentPosition++;
					}
					else if (sServo.uiCurrentPosition>sServo.uiDesiredPosition)
					{
						LedStepLeft();
						sServo.uiCurrentPosition--;
					}
					else if (sServo.uiCurrentPosition==sServo.uiDesiredPosition)
					{
						sServo.eState=IDLE;
					}
					break;
				
				case CALLIB:
					if(eReadDetector() == INACTIVE) 
					{
						LedStepRight();
					} 
					else 
					{
						sServo.eState = IDLE;
						sServo.uiCurrentPosition=0;
						sServo.uiDesiredPosition=0;
						
					}
					break;
					
			}
} 

void ServoInit(unsigned int uiServoFrequency){
	sServo.eState=CALLIB;
	Ledinit();
	Timer0Interrupts_Init(1000000/uiServoFrequency, &Automat);
}

void ServoCallib(void){
	sServo.eState=CALLIB;
}

void ServoGoTo(unsigned int uiPosition){
	sServo.uiDesiredPosition=uiPosition;
}

